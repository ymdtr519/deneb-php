<?php
function is_test_env() {
    $dev_env = getenv('DEV_ENV');
    if ($dev_env == 'true') {
        return true;
    } else {
        return false;
    }
}

function setPdo()
{    
    if (is_test_env()) {
        // $db_host = 'mysql:host=localhost; dbname=deoxi; charset=utf8';
        // $db_user = 'deoxi_user';
        // $db_pass = 'mysql';
        $db_host = 'mysql:host=localhost; dbname=phpy_sample; charset=utf8';
        $db_user = 'phpy_sample';
        $db_pass = 'mysql';
    } else {
        $db_host = '';
        $db_user = '';
        $db_pass = '';
    }
    try {
        $pdo = new PDO(
            $db_host,
            $db_user,
            $db_pass,
            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
        );
        return $pdo;
    } catch (PDOException $e) {
        error_log($e->getMessage());
        http_response_code(500);
        exit(json_encode(array('message' => 'connection failure')));
    }
}
