<?php
namespace deneb;

use PDO;
require_once('./pdo.php');

function notSupportedExit() {
    error_log('サポートされていない形式です');
    exit();
}

function getDefaultValueByType(string $x): string {
    switch ($x) {
        case strpos($x, 'int') !== false : return '0';
        case strpos($x, 'double') !== false : return '0.0';
        case strpos($x, 'varchar') !== false : return  "''";
        case strpos($x, 'longtext') !== false : return "''";
        case strpos($x, 'date') !== false : return 'current_date';
        case strpos($x, 'time') !== false : return 'current_time';
        case strpos($x, 'datetime') !== false : return 'current_timestamp';
        default : notSupportedExit();
    }
}

function genColumnSql(string|array $field): string {
    if (gettype($field) == 'string') {
        return "{$field} varchar(30) not null default ''";
    } else if (gettype($field) == 'array') {
        $default_val = getDefaultValueByType($field[1]);
        return "{$field[0]} {$field[1]} not null default {$default_val}";
    } else {
        notSupportedExit();
    }
}

function getFieldName(string|array $field): string {
    if (gettype($field) == 'string') {
        return $field;
    } else if (gettype($field) == 'array') {
        return $field[0];
    } else {
        notSupportedExit();
    }
}

function checkDb(string $tname, iterable $fields) {
    error_log("In BaseClass constructor");
    error_log("=======================");
    error_log("Table name: $tname");
    error_log("=======================");
    error_log("Field names:");
    foreach ($fields as $fld) {
        $result = genColumnSql($fld);
        error_log($result);
    }
    error_log("=======================");
    // check table
    if (checkTableExist($tname)) {
        error_log('Table exist: OK');
    } else {
        error_log('Table exist: NO');
        createTable($tname, $fields);
        error_log('Table was created');
    }
    // check columns
    error_log("=======================");
    $columns = getColumnNames($tname);
    foreach ($fields as $fld) {
        error_log("---");
        $fname = getFieldName($fld);
        if (in_array($fname, $columns)) {
            error_log("Field name {$fname}: OK");
        } else {
            error_log("Field name {$fname}: New Field!");
            addColumn($tname, $fld);
            error_log("Column was added!");
        }
    }
}

function checkTableExist(string $tname): bool {
    try{
        $pdo = setPdo();
        $sql = "show tables";
        $query = $pdo->query($sql);
        $data = $query->fetchAll(PDO::FETCH_COLUMN);
        return in_array($tname,$data);
    }catch(PDOException $error) {
        return false; 
    }
}

function createTable(string $tname, array $fnames) {
    try{
        $pdo = setPdo();
        $sql_columns_part = implode(",\n", array_map(fn($x) => genColumnSql($x), $fnames));

        $sql = "create table {$tname} (\n"
            . "id integer primary key auto_increment,\n"
            . "{$sql_columns_part},\n"
            . "create_date timestamp not null default current_timestamp,\n"
            . "update_date timestamp not null default current_timestamp on update current_timestamp)";
        error_log("SQL: {$sql}");

        $pdo->exec($sql);
                                                                        
        echo "Table created successfully";
    }catch(PDOException $error) {
        echo "Table creation failed: " . $e->getMessage();
    }
}

function getColumnNames(string $tname): array {
    $pdo = setPdo();
    $sql = "show columns from {$tname}";
    $query = $pdo->query($sql);
    $data = $query->fetchAll(PDO::FETCH_COLUMN);
    return $data;
}

function addColumn(string $tname, string|array $field) {
    $cname = getFieldName($field);
    $type = '';
    $defaultVal = '';
    if (gettype($field) == 'string') {
        $type = 'varchar(255)';
        $defaultVal = "''";
    } else if (gettype($field) == 'array') {
        $type = $field[1];
        $defaultVal = getDefaultValueByType($field[1]);
    } else {
        notSupportedExit();
    }
    try{
        $pdo = setPdo();

        $columns = getColumnNames($tname);
        $lastContentColumnName = array_reverse(getColumnNames($tname))[2];
        $sql = "alter table {$tname} add {$cname} {$type} default {$defaultVal} after {$lastContentColumnName}";
        error_log("SQL: {$sql}");

        $pdo->exec($sql);
                                                                        
        echo "Table created successfully";
    }catch(PDOException $error) {
        echo "Table creation failed: " . $e->getMessage();
    }
}
